package fr.univLille.r304.tarification;

public class PrixParUnite extends AbstractPrixSelonUnite {
	public PrixParUnite(Prix base, EUnite unit) {
		super( unit, base);
	}
	public PrixParUnite(double base, EUnite unit) {
		super( unit, base);
	}

	@Override
	public double calcul(Quantite realise) {
		if (realise == null) {
			return 0.0;
		}
		if (realise.getUnit() == unite) {
    		// note: devrait tenir compte de l'inclusin des unités: 1 SEMAINE=7 JOURS; 1 JOUR=24 HEURES, ...
			return base.calcul(realise) * realise.getQte();
		}
		return 0.0;
	}

}

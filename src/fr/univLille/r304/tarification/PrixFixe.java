package fr.univLille.r304.tarification;


public class PrixFixe extends Prix {
    protected double fixe;

    public PrixFixe(double fixe) {
        this.fixe = fixe;
    }
    public double calcul(Quantite realise) {
        return fixe;
    }
}
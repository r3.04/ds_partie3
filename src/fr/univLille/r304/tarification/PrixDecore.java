package fr.univLille.r304.tarification;

public abstract class PrixDecore extends Prix {
    protected Prix base;
    public PrixDecore(Prix base) {
        this.base = base;
    }
    /**
     * Constructeur alternatif ou le brix de <code>base</code> est un <code>double</code> (converti en <code>PrixFixe</code>)
     */
    public PrixDecore(double base) {
        this.base = new PrixFixe(base);
    }
    
}

package fr.univLille.r304.tarification;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestPrixFixe extends AbstractTestPrix {
	@BeforeEach
	void setup(){
		this.prix = new PrixFixe(5);
	}
	@Test
	void testSansRealise() {
		assertEquals(5, prix.calcul(null));
	}

	@Test
	void testUneUnite() {
		assertEquals(5, prix.calcul(new Quantite(10, EUnite.HEURE)));
	}

}

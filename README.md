
Proposition de solution pour la partie 3 du DS de R3.04

# Question 3.1

## Loueur de voiture
- 120 € les 2 premiers jours ;
- puis 50 € par jour.

```Java
new PrixJusqua(2, EUnite.JOUR,
    new PrixFixe(120.0), 
    new PrixParUnite(new PrixFixe(50.0), EUnite.JOUR));
```

## Horodateur
- 15 premières minutes gratuites ;
- puis 2 € de l’heure pendant 4 heures ;
- puis 30 € par jour.

```Java
Prix parJour = new PrixParUnite(30.0, EUnite.JOUR);
Prix prixALHeure = new PrixJusqua(  4, EUnite.HEURE, new PrixParUnite( 2.0, EUnite.HEURE), parJour);
new PrixJusqua( 15, EUnite.MINUTE, 0.0, prixALHeure);
```

# Question 3.2

Diagramme UML généré avec [PlantUML](http://www.plantuml.com/).
- Les deux classes abstraites sont le "coeur" du patron Décorteur.
- la classe `PrixFixe` est la "feuille" du patron (classe non décorée).
- Les classes `PrixJusquà` et `PrixParUnité` sont des décorateurs sur des `Prix`.

![diagramme de classe](./doc/diagClasse.png)

Note: "code" pour la génération du diagramme :
```
@startuml
hide empty members
abstract class Prix {
{abstract} double calcul(réalisé);
}
class PrixFixe {
double prix;
}
abstract class PrixDécoré {
Prix prixBase;
}
class PrixJusquà {
int max;
Unité unité;
Prix prixSinon;
}
class PrixParUnité {
Unité unité;
}
Prix <|-- PrixFixe
Prix <|-- PrixDécoré
PrixDécoré o- Prix
PrixDécoré <|-- PrixJusquà
PrixDécoré <|-- PrixParUnité
@enduml
```

package fr.univLille.r304.tarification;

/**
 * Classe abstraite racine du patron décorateur
 * <p>
 * Question 3.1 du DS
 * <p>
 * Loueur de voiture
 * <ul>
 * <li>120 € les 2 premiers jours ;</li>
 * <li>puis 50 € par jour.</li>
 * </ul>
 * <code>new PrixJusqua(2, EUnite.JOUR, new PrixFixe(120.0), new PrixParUnite(new PrixFixe(50.0), EUnite.JOUR));</code>
 * <p>
 * Horodateur
 * <ul>
 * <li>15 premières minutes gratuites ;</li>
 * <li>puis 2 € de l’heure pendant 4 heures ;</li>
 * <li>puis 30 € par jour.</li>
 * </ul>
 * <code>Prix parJour = new PrixParUnite(30.0, EUnite.JOUR);<br>
 * Prix prixALHeure = new PrixJusqua(  4, EUnite.HEURE, new PrixParUnite( 2.0, EUnite.HEURE), parJour);<br>
 * new PrixJusqua( 15, EUnite.MINUTE, 0.0, prixALHeure);</code>
 */
public abstract class Prix {
	/**
	 * Calcul le prix a payé pour une <code>Quantité</code> réalisée
	 */
    public abstract double calcul(Quantite realise);
}
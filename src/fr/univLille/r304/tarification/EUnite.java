package fr.univLille.r304.tarification;

public enum EUnite {
    MOIS, SEMAINE, JOUR, HEURE, MINUTE, KILOMETRE;

}
package fr.univLille.r304.tarification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestPrixParUnite extends AbstractTestPrix {
    @BeforeEach
	void setup(){
		this.prix = new PrixParUnite( new PrixFixe(5), EUnite.MINUTE);
	}
	@Test
	void testSansRealise() {
		assertEquals(0, prix.calcul(null));
	}

	@Test
	void testMemeUnite() {
		assertEquals(50, prix.calcul(new Quantite(10, EUnite.MINUTE)));
	}

	@Test
	void testAutreUnite() {
		assertEquals(0, prix.calcul(new Quantite(10, EUnite.HEURE)));
	}

}

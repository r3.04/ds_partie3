package fr.univLille.r304.tarification;

/**
 * Spécifie le nombre d'unités réalisées (ex: 15 HEUREs).
 * Utilisé pour calculer le prix du en fonction de la quantité réalisée
 */
public class Quantite {

		private EUnite unit;
		private int qte;

		public Quantite(int qte, EUnite unite) {
			this.unit = unite;
			this.qte = qte;
		}

		public EUnite getUnit() {
			return unit;
		}

		public int getQte() {
			return qte;
		}
}

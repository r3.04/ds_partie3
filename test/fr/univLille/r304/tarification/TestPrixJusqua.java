package fr.univLille.r304.tarification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestPrixJusqua extends AbstractTestPrix {
    @BeforeEach
	void setup(){
		this.prix = new PrixJusqua( 30, EUnite.MINUTE, 5.0, 15.0);
	}
	@Test
	void testSansRealise() {
		assertEquals(0, prix.calcul(null));
	}

	@Test
	void testMemeUniteInferieurMax() {
		assertEquals(5, prix.calcul(new Quantite(10, EUnite.MINUTE)));
	}

	@Test
	void testMemeUniteSuperieurMax() {
		assertEquals(15, prix.calcul(new Quantite(100, EUnite.MINUTE)));
	}

	@Test
	void testAutreUnite() {
		assertEquals(15.0, prix.calcul(new Quantite(25, EUnite.HEURE)));
	}

}

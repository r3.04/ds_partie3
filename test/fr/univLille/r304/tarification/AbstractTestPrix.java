package fr.univLille.r304.tarification;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractTestPrix {
    protected Prix prix;

	protected Collection<Quantite> unitList(Quantite... qtes) {
		Collection<Quantite> retn = new ArrayList<>( );
		for (Quantite q : qtes) {
			retn.add(q);
		}
		return retn;
	}
}

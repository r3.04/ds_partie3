package fr.univLille.r304.tarification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestTarificationComplexe extends AbstractTestPrix {
    @BeforeEach
	void setup(){
		// 15 minutes gratuites
		// puis 2.00 de l'heure jusqu'a 4 heures
		// puis 30.00 par jour
		Prix parJour = new PrixParUnite(30.0, EUnite.JOUR);
		Prix prixALHeure = new PrixJusqua(  4, EUnite.HEURE, new PrixParUnite( 2.0, EUnite.HEURE), parJour);
		this.prix = new PrixJusqua( 15, EUnite.MINUTE, 0.0, prixALHeure);
	}

	@Test
	void test15Mn() {
		assertEquals(0.0, prix.calcul(new Quantite(15, EUnite.MINUTE)));
	}

	@Test
	void test2Hrs() {
		assertEquals(4.0, prix.calcul(new Quantite(2, EUnite.HEURE)));
	}

	@Test
	void test3Jrs() {
		assertEquals(90.0, prix.calcul(new Quantite(3, EUnite.JOUR)));
	}

}

package fr.univLille.r304.tarification;

import java.util.Collection;

public abstract class AbstractPrixSelonUnite extends PrixDecore {

	protected EUnite unite;

	public AbstractPrixSelonUnite(EUnite unit, Prix base) {
		super(base);
        this.unite = unit;
	}
	public AbstractPrixSelonUnite(EUnite unit, double base) {
		super(base);
        this.unite = unit;
	}

}
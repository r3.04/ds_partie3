package fr.univLille.r304.tarification;

public class PrixJusqua extends AbstractPrixSelonUnite {
    protected int max;
    protected Prix prixSinon;
    /**
     * Jusqu'a <code>max</code> <code>unit</code>, prix de <code>base</code>, sinon, <code>prixSinon</code>
     */
    public PrixJusqua(int max, EUnite unit, Prix base, Prix prixSinon) {
        super(unit, base);
        this.max = max;
        this.prixSinon = prixSinon;
    }
    /**
     * Constructeur alternatif ou le prix de <code>base</code> est un <code>double</code> (converti en PrixFixe)
     */
    public PrixJusqua(int max, EUnite unit, double base, Prix prixSinon) {
        super(unit, base);
        this.max = max;
        this.prixSinon = prixSinon;
    }
    /**
     * Constructeur alternatif ou le <code>prixSinon</code> est un <code>double</code> (converti en PrixFixe)
     */
    public PrixJusqua(int max, EUnite unit, Prix base, double prixSinon) {
        this(max, unit, base, new PrixFixe(prixSinon));
    }
    /**
     * Constructeur alternatif ou le prix de <code>base</code> et le <code>prixSinon</code> sont des <code>double</code> (convertis en PrixFixe)
     */
    public PrixJusqua(int max, EUnite unit, double base, double prixSinon) {
        this(max, unit, base, new PrixFixe(prixSinon));
    }

    @Override
    public double calcul(Quantite realise) {
    	if (realise == null) {
    		return 0;
    	}
    	if ( (realise.getUnit() == unite) && (realise.getQte() <= max) ) {
    		// note: devrait tenir compte de l'inclusin des unités: 1 SEMAINE=7 JOURS; 1 JOUR=24 HEURES, ...
    		return base.calcul(realise);
    	}
    	else {
    		return prixSinon.calcul(realise);
    	}
    }

}
